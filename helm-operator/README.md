Operator for Helm to install kubernetes stuff using helm without local helm command installation.

## Installation ##
First of all we need to install CRD's for defining HelmReleases

To do it we should apply it's configuration:
```
kubectl apply -f operators-crds.yaml
```

Then we need to create namespace for it:
```
kubectl create ns helm
```

Then we need to create RBAC rules:
```
kubectl apply -f operators-rbac.yaml
```

And finally make deployment:
```
kubectl apply -f operators-deployment.yaml
